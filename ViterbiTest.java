import static org.junit.Assert.*;
import com.google.common.base.Stopwatch;
import edu.princeton.cs.algs4.In;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

public class ViterbiTest {

	private final static double EPSILON = Math.pow(10, -15);
	private Viterbi v;
	private double[] pi;
	private double[][] A;
	private double[][] B;
	
	private ByteArrayOutputStream outContent;
	@Before // initialize ByteArrayOutputStream for printing test
	public void stream() {
		outContent = new ByteArrayOutputStream();
		System.setOut(new PrintStream(outContent));
	}
	
	@Before 
	public void setup() {
		pi = new double[2];
		A = new double[2][2];
		B = new double[2][2];
		pi[0] = 0.45; pi[1] = 0.55;
		A[0][0] = 0.7; A[0][1] = 0.3; A[1][0] = 0.25; A[1][1] = 0.75;
		B[0][0] = 0.8; B[0][1] = 0.2; B[1][0] = 0.3; B[1][1] = 0.7;
		
		v = new Viterbi(pi, A, B);
	}
	
	/******************************** PROBLEM STATEMENT PART (1) *********************************/
	
	@Test // Part (1) - Omega 1
	public void omega1() {
		int[] omega = {0, 0, 0, 1};
		assertEquals(0.023708159999999995, v.maxScore(omega, false), EPSILON);
		assertEquals(-3.7419359863025345, v.maxScore(omega, true), EPSILON);
		List<Integer> bestSeq = v.bestSeq(omega, false);
		List<Integer> expected = new ArrayList<>(Arrays.asList(0,0,0,1));
		assertTrue(bestSeq.equals(expected));
	}
	
	@Test // Part (1) - Omega 2
	public void omega2() {
		int[] omega = {0, 0, 0, 1, 0};
		assertEquals(0.0088510464, v.maxScore(omega, false), EPSILON);
		List<Integer> bestSeq = v.bestSeq(omega, false);
		List<Integer> expected = new ArrayList<>(Arrays.asList(0,0,0,0,0));
		assertTrue(bestSeq.equals(expected));
	}
	
	@Test // Part (1) - Omega 3
	public void omega3() {
		int[] omega = {0, 0, 0, 1, 0, 0};
		assertEquals(0.004956585984, v.maxScore(omega, false), EPSILON);
		List<Integer> bestSeq = v.bestSeq(omega, false);
		List<Integer> expected = new ArrayList<>(Arrays.asList(0,0,0,0,0,0));
		assertTrue(bestSeq.equals(expected));
	}
	
	@Test // Part (1) - Omega 4
	public void omega4() {
		int[] omega = {0, 0, 0, 1, 0, 1, 1, 0};
		assertEquals(3.3081218099999986E-4, v.maxScore(omega, false), EPSILON);
		List<Integer> bestSeq = v.bestSeq(omega, false);
		List<Integer> expected = new ArrayList<>(Arrays.asList(0,0,0,1,1,1,1,1));
		assertTrue(bestSeq.equals(expected));
	}
	
	@Test // Part (1) - Omega 5
	public void omega5() {
		int[] omega = new int[1200];
		for (int i = 0; i < 300; i++) 	 omega[i] = 0;
		for (int i = 300; i < 600; i++)  omega[i] = 1;
		for (int i = 600; i < 900; i++)  omega[i] = 0;
		for (int i = 900; i < 1200; i++) omega[i] = 1;
		assertEquals(3.74E-321, v.maxScore(omega, false), EPSILON);
		assertEquals(omega.length, v.bestSeq(omega, false).size());
	}
	
	@Test // Part (1) - Omega 6
	public void omega6() {
		int[] omega = new int[2000];
		for (int i = 0; i < 500; i++) 	  omega[i] = 0;
		for (int i = 500; i < 1000; i++)  omega[i] = 1;
		for (int i = 1000; i < 1500; i++) omega[i] = 0;
		for (int i = 1500; i < 2000; i++) omega[i] = 1;
		assertEquals(0, v.maxScore(omega, false), EPSILON);
		assertEquals(omega.length, v.bestSeq(omega, false).size());
	}
	
	@Test // Part (1) - Omega 7
	public void omega7() {
		int[] omega = new int[2004];
		for (int i = 0; i < 500; i++) 	  omega[i] = 0;
		for (int i = 500; i < 1000; i++)  omega[i] = 1;
		for (int i = 1000; i < 1500; i++) omega[i] = 0;
		for (int i = 1500; i < 2000; i++) omega[i] = 1;
		omega[2000] = 0; omega[2001] = 0; omega[2002] = 0; omega[2003] = 1; 
		assertEquals(0, v.maxScore(omega, false), EPSILON);
		assertEquals(omega.length, v.bestSeq(omega, false).size());
	}
	
	/******************************** PROBLEM STATEMENT PART (2) *********************************/
	
	@Test // Part (2) - Omega 5
	public void omega5Log() {
		int[] omega = new int[1200];
		for (int i = 0; i < 300; i++) 	 omega[i] = 0;
		for (int i = 300; i < 600; i++)  omega[i] = 1;
		for (int i = 600; i < 900; i++)  omega[i] = 0;
		for (int i = 900; i < 1200; i++) omega[i] = 1;
		assertEquals(-737.8093406192771, v.maxScore(omega, true), EPSILON);
		assertEquals(omega.length, v.bestSeq(omega, true).size());
	}
	
	@Test // Part (2) - Omega 6
	public void omega6Log() {
		int[] omega = new int[2000];
		for (int i = 0; i < 500; i++) 	  omega[i] = 0;
		for (int i = 500; i < 1000; i++)  omega[i] = 1;
		for (int i = 1000; i < 1500; i++) omega[i] = 0;
		for (int i = 1500; i < 2000; i++) omega[i] = 1;
		assertEquals(-1227.4795452767084, v.maxScore(omega, true), EPSILON);
		assertEquals(omega.length, v.bestSeq(omega, true).size());
	}
	
	@Test // Part (2) - Omega 7
	public void omega7Log() {
		int[] omega = new int[2004];
		for (int i = 0; i < 500; i++) 	  omega[i] = 0;
		for (int i = 500; i < 1000; i++)  omega[i] = 1;
		for (int i = 1000; i < 1500; i++) omega[i] = 0;
		for (int i = 1500; i < 2000; i++) omega[i] = 1;
		omega[2000] = 0; omega[2001] = 0; omega[2002] = 0; omega[2003] = 1; 
		assertEquals(-1231.8092679279132, v.maxScore(omega, true), EPSILON);
		assertEquals(omega.length, v.bestSeq(omega, true).size());
	}
	
	/******************************** PROBLEM STATEMENT PART (3) *********************************/
	
	@Test // Part (3) - Omega 1
	public void DNA1() {
		double[] pi = {0.5, 0.5};
		A = new double[2][2];
		B = new double[2][4];
		A[0][0] = 0.5; A[0][1] = 0.5; A[1][0] = 0.4; A[1][1] = 0.6;
		B[0][0] = 0.2; B[0][1] = 0.3; B[0][2] = 0.3; B[0][3] = 0.2; 
		B[1][0] = 0.3; B[1][1] = 0.2; B[1][2] = 0.2; B[1][3] = 0.3;
		v = new Viterbi(pi, A, B);

		int[] omega = {2, 2, 1, 0, 1, 3, 2, 0, 0};
		
		assertEquals(4.2515279999999984E-8, v.maxScore(omega, false), EPSILON);
		assertEquals(-16.973402296219486, v.maxScore(omega, true), EPSILON);
		
		List<Integer> bestSeq = v.bestSeq(omega, false);
		ArrayList<Integer> expected = new ArrayList<>(Arrays.asList(0,0,0,1,1,1,1,1,1));
		assertTrue(bestSeq.equals(expected));
		bestSeq = v.bestSeq(omega, true);
		assertTrue(bestSeq.equals(expected));
	}
	
	@Test // Part (3) - Omega 2
	public void DNA2() {
		double[] pi = {0.5, 0.5};
		A = new double[2][2];
		B = new double[2][4];
		A[0][0] = 0.5; A[0][1] = 0.5; A[1][0] = 0.4; A[1][1] = 0.6;
		B[0][0] = 0.2; B[0][1] = 0.3; B[0][2] = 0.3; B[0][3] = 0.2; 
		B[1][0] = 0.3; B[1][1] = 0.2; B[1][2] = 0.2; B[1][3] = 0.3;
		v = new Viterbi(pi, A, B);
		
		int[] omega = {2, 0, 2, 0, 3, 0, 3, 0, 1, 0, 3, 0, 2, 0, 0, 3, 3, 0, 1, 2};
		ArrayList<Integer> expected = new ArrayList<>(Arrays.asList(0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,0));
		
		assertEquals(1.4572743731591294E-16, v.maxScore(omega, false), EPSILON);
		assertEquals(-36.46479366465307, v.maxScore(omega, true), EPSILON);
		
		List<Integer> bestSeq = v.bestSeq(omega, false);
		assertTrue(bestSeq.equals(expected));
		bestSeq = v.bestSeq(omega, true);
		assertTrue(bestSeq.equals(expected));
		
		assertEquals(1.4572743731591294E-16, v.probOfSeq(omega, expected, false), EPSILON);
		assertEquals(-36.464793664653065, v.probOfSeq(omega, expected, true), EPSILON);
	}
	
	/**************************************** METHOD TESTS ***************************************/
	
	@Test // Class example worst sequence
	public void worstCaseSeq() {
		int[] omega = {0, 0, 0, 1};
		List<Integer> seq = new LinkedList<>();
		seq.add(1); seq.add(0); seq.add(1); seq.add(0);
		assertEquals(seq, v.worstSeq(omega, false));
		assertEquals(1.485E-4, v.probOfSeq(omega, seq, false), EPSILON);
	}
	
	@Test // Example given in class
	public void testMaxScoreSet() {
		int[] omega = {0, 0, 0, 1};
		double[] set = v.maxScoreSet(omega, 2, false);
		assertEquals(0.023708159999999995, set[0], EPSILON);
		assertEquals(0.01580544, set[1], EPSILON);
	}
	
	@Test // hidden probabilities - naive version
	public void testMaxScoreSet2() {
		double[] pi = {0.9, 0.1};
		A[0][0] = 0.9; A[0][1] = 0.1; A[1][0] = 0.8; A[1][1] = 0.2;
		B[0][0] = 0.4; B[0][1] = 0.6; B[1][0] = 0.95; B[1][1] = 0.05;
		v = new Viterbi(pi, A, B);
		
		int[] omega = {0, 1};
		double[] set = v.maxScoreSet(omega, 2, false);
		assertEquals(0.19440000000000004, set[0], EPSILON);
		assertEquals(0.0018000000000000004, set[1], EPSILON);
	}
	
	@Test // hidden probabilities - EC version
	public void testMaxScoreSetEC() {
		double[] pi = {0.9, 0.1};
		A[0][0] = 0.9; A[0][1] = 0.1; A[1][0] = 0.8; A[1][1] = 0.2;
		B[0][0] = 0.4; B[0][1] = 0.6; B[1][0] = 0.95; B[1][1] = 0.05;
		v = new Viterbi(pi, A, B);
		
		int[] omega = {0, 1};
		double[] set = v.maxScoreSetEC(omega, 2, false);
		assertEquals(0.19440000000000004, set[0], EPSILON);
		assertEquals(0.04560000000000001, set[1], EPSILON);
	}
	
	@Test // probability of any path is 0.0 (or -Infinity when underflow) 
	public void maxScoreSetEmpty() {
		double[] pi = {0.5, 0.5};
		A = new double[2][2];
		B = new double[2][2];
		A[0][0] = 1; A[0][1] = 0; A[1][0] = 1; A[1][1] = 0;
		B[0][0] = 1; B[0][1] = 0; B[1][0] = 1; B[1][1] = 0;
		v = new Viterbi(pi, A, B);

		int[] omega = {0, 0, 1, 1};
		assertEquals(0, v.maxScoreSet(omega, 1, false)[0], EPSILON);
		assertEquals(Double.NEGATIVE_INFINITY, v.maxScoreSet(omega, 1, true)[0], EPSILON);
	}
	
	@Test // sequences have same probabilities
	public void maxAndBestSeqSetSameProbs() {
		double[] pi = {0.5, 0.5};
		A = new double[2][2];
		B = new double[2][2];
		A[0][0] = 0.5; A[0][1] = 0.5; A[1][0] = 0.5; A[1][1] = 0.5;
		B[0][0] = 0.5; B[0][1] = 0.5; B[1][0] = 0.5; B[1][1] = 0.5;
		v = new Viterbi(pi, A, B);

		int[] omega = {0, 0, 0, 0};
		v.bestSeq(omega, false);
		v.bestSeqSet(omega, false);
		v.maxScoreSet(omega, 1, false);
		v.maxScoreSet(omega, 1, true);  	// IF ONLY -Infinity WHAT SHOULD WE RETURN?
	}
	
	@Test // bestSeqSet with every path having 0 probability
	public void bestSeqSet() {
		double[] pi = {0.5, 0.5};
		A = new double[2][2];
		B = new double[2][2];
		A[0][0] = 0; A[0][1] = 1; A[1][0] = 0; A[1][1] = 1;
		B[0][0] = 1; B[0][1] = 0; B[1][0] = 1; B[1][1] = 0;
		v = new Viterbi(pi, A, B);
		int[] omega = {0, 0, 1, 1};
		
		List<List<Integer>> l = v.bestSeqSet(omega, false);	
		List<List<Integer>> l2 = v.bestSeqSet(omega, true);	
		assertEquals("l should be empty because all paths have probability of 0", 0, l.size());
		assertEquals("l should be empty because all paths have probability of 0", 0, l2.size());
		assertTrue(0 == v.maxScore(omega, false));
		assertTrue(Double.NEGATIVE_INFINITY == v.maxScore(omega, true));
		
		// bestSeq and worstSeq should take the lowest path
		ArrayList<Integer> expected = new ArrayList<>(Arrays.asList(0,0,0,0));
		assertTrue(v.bestSeq(omega, true).equals(expected));
		assertTrue(v.bestSeq(omega, false).equals(expected));
		assertTrue(v.worstSeq(omega, true).equals(expected));
		assertTrue(v.worstSeq(omega, false).equals(expected));
		
		expected = new ArrayList<>(Arrays.asList(0,0,0,1));
		assertTrue("Probability of occurring is zero", 0 == v.probOfSeq(omega, expected, false));
		assertTrue("Probability of occurring is -Infinity", 
				   Double.NEGATIVE_INFINITY == v.probOfSeq(omega, expected, true));
	}
	
	@Test // one column has 0 and others have 1 probabilities of transition
	public void bestSeqZero() {
		double[] pi = {(double) 1/3, (double) 1/3, (double) 1/3};
		A = new double[3][3];
		A[0][0] = 0; A[0][1] = 0; A[0][2] = 1;
		A[1][0] = 0; A[1][1] = 0; A[1][2] = 1;
		A[2][0] = 0; A[2][1] = 0; A[2][2] = 1;
		B = new double[3][3];
		B[0][0] = (double) 1/3; B[0][1] = (double) 1/3; B[0][2] = (double) 1/3;
		B[1][0] = (double) 1/3; B[1][1] = (double) 1/3; B[1][2] = (double) 1/3;
		B[2][0] = (double) 1/3; B[2][1] = (double) 1/3; B[2][2] = (double) 1/3;
		v = new Viterbi(pi, A, B);
		int[] omega = {0, 0, 0, 0};
		
		ArrayList<Integer> expected = new ArrayList<>(Arrays.asList(0,2,2,2));
		assertTrue(v.bestSeq(omega, false).equals(expected));
		assertTrue(v.bestSeq(omega, true).equals(expected));

		expected = new ArrayList<>(Arrays.asList(0,0,0,0));
		assertTrue(v.worstSeq(omega, false).equals(expected));
		assertTrue(v.worstSeq(omega, true).equals(expected));
		
		List<ArrayList<Integer>> l = new ArrayList<ArrayList<Integer>>();
		ArrayList<Integer> a1 = new ArrayList<>(Arrays.asList(0,2,2,2));
		l.add(a1); 
		
		assertTrue(v.bestSeqSet(omega, false).equals(l));
		assertTrue(v.bestSeqSet(omega, true).equals(l));
		
		assertEquals(0.004115226337448559, v.maxScoreSet(omega, 1, false)[0], EPSILON);
		assertEquals(-5.493061443340549, v.maxScoreSet(omega, 1, true)[0], EPSILON);
		
		ArrayList<Integer> low = new ArrayList<>(Arrays.asList(0,2,2,2));
		ArrayList<Integer> high = new ArrayList<>(Arrays.asList(2,2,2,2));
		assertTrue(v.probOfSeq(omega, low, false) == v.probOfSeq(omega, high, false));
	}
	
	@Test // one column has 0 and another has 1 probability of transition
	public void bestSeqZero2() {
		double[] pi = {(double) 1/2, (double) 1/2};
		A = new double[2][2];
		A[0][0] = 0; A[0][1] = 1;
		A[1][0] = 0; A[1][1] = 1;
		B = new double[2][3];
		B[0][0] = (double) 1/3; B[0][1] = (double) 1/3; B[0][2] = (double) 1/3;
		B[1][0] = (double) 1/3; B[1][1] = (double) 1/3; B[1][2] = (double) 1/3;
		v = new Viterbi(pi, A, B);
		int[] omega = {0, 0, 0, 0};
		
		ArrayList<Integer> expected = new ArrayList<>(Arrays.asList(0,1,1,1));
		assertTrue(v.bestSeq(omega, false).equals(expected));
		assertTrue(v.bestSeq(omega, true).equals(expected));

		expected = new ArrayList<>(Arrays.asList(0,0,0,0));
		assertTrue(v.worstSeq(omega, false).equals(expected));
		assertTrue(v.worstSeq(omega, true).equals(expected));
		
		List<ArrayList<Integer>> l = new ArrayList<ArrayList<Integer>>();
		ArrayList<Integer> a1 = new ArrayList<>(Arrays.asList(0,1,1,1));
		l.add(a1);
		
		assertTrue(v.bestSeqSet(omega, false).equals(l));
		assertTrue(v.bestSeqSet(omega, true).equals(l));

		double[] max = v.maxScoreSet(omega, 2, true);
		assertEquals(-5.0875963352323845, max[0], EPSILON);
		assertEquals(Double.NEGATIVE_INFINITY, max[1], EPSILON);
		
		max = v.maxScoreSet(omega, 2, false);
		assertEquals(0.006172839506172839, max[0], EPSILON);
		assertEquals(0.0, max[1], EPSILON);
	}
	
	@Test
	public void maxScoreSetNaiveAndEC() {
		double[] pi = {(double) 1/3, (double) 1/3, (double) 1/3};
		A = new double[3][3];
		A[0][0] = (double) 1/3; A[0][1] = (double) 1/3; A[0][2] = (double) 1/3;
		A[1][0] = (double) 1/3; A[1][1] = (double) 1/3; A[1][2] = (double) 1/3;
		A[2][0] = (double) 1/3; A[2][1] = (double) 1/3; A[2][2] = (double) 1/3;
		B = new double[3][3];
		B[0][0] = (double) 1/3; B[0][1] = (double) 1/3; B[0][2] = (double) 1/3;
		B[1][0] = (double) 1/3; B[1][1] = (double) 1/3; B[1][2] = (double) 1/3;
		B[2][0] = (double) 1/3; B[2][1] = (double) 1/3; B[2][2] = (double) 1/3;
		v = new Viterbi(pi, A, B);
		int[] omega = {0, 0, 0, 0};
		
		double[] max = v.maxScoreSet(omega, 1, false);
		assertTrue("Only 1 probability", 1 == max.length);
		max = v.maxScoreSetEC(omega, 1, false);
		assertTrue("Only 1 probability", 1 == max.length);
		max = v.maxScoreSetEC(omega, 1, true);
		assertTrue("Only 1 probability", 1 == max.length);
	}
	
	@Test // bestSeq with ties - takes the lowest index path
	public void bestSeq() {
		double[] pi = {(double) 1/3, (double) 1/3, (double) 1/3};
		A = new double[3][3];
		A[0][0] = (double) 1/3; A[0][1] = (double) 1/3; A[0][2] = (double) 1/3;
		A[1][0] = (double) 1/3; A[1][1] = (double) 1/3; A[1][2] = (double) 1/3;
		A[2][0] = (double) 1/3; A[2][1] = (double) 1/3; A[2][2] = (double) 1/3;
		B = new double[3][3];
		B[0][0] = (double) 1/3; B[0][1] = (double) 1/3; B[0][2] = (double) 1/3;
		B[1][0] = (double) 1/3; B[1][1] = (double) 1/3; B[1][2] = (double) 1/3;
		B[2][0] = (double) 1/3; B[2][1] = (double) 1/3; B[2][2] = (double) 1/3;
		v = new Viterbi(pi, A, B);
		int[] omega = {0, 1, 1, 2, 1, 0, 0};
		
		ArrayList<Integer> expected = new ArrayList<>(Arrays.asList(0,0,0,0,0,0,0));
		assertTrue(v.bestSeq(omega, false).equals(expected));
		assertTrue(v.bestSeq(omega, true).equals(expected));
	}
	
	@Test // bestSeqSet with ties - takes the lowest and highest index paths
	public void bestSeqSetTie() {
		double[] pi = {(double) 1/3, (double) 1/3, (double) 1/3};
		A = new double[3][3];
		A[0][0] = (double) 1/3; A[0][1] = (double) 1/3; A[0][2] = (double) 1/3;
		A[1][0] = (double) 1/3; A[1][1] = (double) 1/3; A[1][2] = (double) 1/3;
		A[2][0] = (double) 1/3; A[2][1] = (double) 1/3; A[2][2] = (double) 1/3;
		B = new double[3][3];
		B[0][0] = (double) 1/3; B[0][1] = (double) 1/3; B[0][2] = (double) 1/3;
		B[1][0] = (double) 1/3; B[1][1] = (double) 1/3; B[1][2] = (double) 1/3;
		B[2][0] = (double) 1/3; B[2][1] = (double) 1/3; B[2][2] = (double) 1/3;
		v = new Viterbi(pi, A, B);
		int[] omega = {0, 1, 1, 2, 1, 0, 0};
		
		List<ArrayList<Integer>> l = new ArrayList<ArrayList<Integer>>();
		ArrayList<Integer> a1 = new ArrayList<>(Arrays.asList(0,0,0,0,0,0,0));
		ArrayList<Integer> a2 = new ArrayList<>(Arrays.asList(2,2,2,2,2,2,2));
		l.add(a1); l.add(a2);
		
		assertTrue(v.bestSeqSet(omega, false).equals(l));
		assertTrue(v.bestSeqSet(omega, true).equals(l));
	}
	
	@Test // Check on large input
	public void largeOmega() {
		double[] pi = {(double) 1/3, (double) 1/3, (double) 1/3};
		A = new double[3][3];
		A[0][0] = (double) 1/3; A[0][1] = (double) 1/3; A[0][2] = (double) 1/3;
		A[1][0] = (double) 1/3; A[1][1] = (double) 1/3; A[1][2] = (double) 1/3;
		A[2][0] = (double) 1/3; A[2][1] = (double) 1/3; A[2][2] = (double) 1/3;
		B = new double[3][3];
		B[0][0] = (double) 1/3; B[0][1] = (double) 1/3; B[0][2] = (double) 1/3;
		B[1][0] = (double) 1/3; B[1][1] = (double) 1/3; B[1][2] = (double) 1/3;
		B[2][0] = (double) 1/3; B[2][1] = (double) 1/3; B[2][2] = (double) 1/3;
		v = new Viterbi(pi, A, B);
		
		In in = new In("Large Omega.txt");
		int[] omega = new int[2005];
		for (int i = 0; i < 2005; i++) {
			omega[i] = in.readInt();
		}
		
		ArrayList<Integer> lowExpect = new ArrayList<Integer>();
		ArrayList<Integer> highExpect = new ArrayList<Integer>();
		for (int i = 0; i < 2005; i++) {
			lowExpect.add(0);
			highExpect.add(2);
		}
		List<ArrayList<Integer>> l = new ArrayList<ArrayList<Integer>>();
		l.add(lowExpect); l.add(highExpect);
		
		assertTrue(v.bestSeqSet(omega, true).equals(l));
		assertEquals("Because of underflow, no log will give 0 probability", 
					 0, v.bestSeqSet(omega, false).size());
		assertTrue(0 == v.maxScore(omega, false));
		assertTrue(v.bestSeq(omega, false).equals(lowExpect));
	}
	
	
	@Test // Check performance using Stopwatch 
	public void testTime() {
		int[] omega = new int[100];
		for (int i = 0; i < omega.length; i++) omega[i] = 0;
		
		pi = new double[500];
		A = new double[500][500];
		B = new double[500][500];
		for (int i = 0; i < A.length; i++) {
			pi[i] = 0.002;
			for (int j = 0; j < A[0].length; j++) {
				A[i][j] = 0.002;
				B[i][j] = 0.002;
			}
		}
		v = new Viterbi(pi, A, B);
		
		Stopwatch stopwatch = Stopwatch.createStarted();
		assertEquals(-1242.9216196844384, v.maxScore(omega, true), EPSILON);
		assertEquals(omega.length, v.bestSeq(omega, true).size());
		stopwatch.stop();
		System.out.println("Two operations on a huge viterbi took: " + stopwatch);
	}
	
	/************************************** EXCEPTION TESTS **************************************/
	
	// A is not a square matrix
	@Test(expected = IllegalArgumentException.class)
	public void viterbiWrongA() {
		A = new double[3][2];
		new Viterbi(pi, A, B);
	}
	
	// B's length does not match A's
	@Test(expected = IllegalArgumentException.class)
	public void viterbiWrongB() {
		B = new double[3][2];
		new Viterbi(pi, A, B);
	}
	
	// A has invalid probability
	@Test(expected = IllegalArgumentException.class)
	public void wrongElementInA() {
		A[0][0] = 1.7; A[0][1] = -1.7;
		new Viterbi(pi, A, B);
	}
	
	// B has invalid probability
	@Test(expected = IllegalArgumentException.class)
	public void wrongElementInB() {
		B[0][0] = 1.7; B[0][1] = -1.7;
		new Viterbi(pi, A, B);
	}
	
	// Sum of probabilities in A's row is not 1
	@Test(expected = IllegalArgumentException.class)
	public void wrongSumOfRowInA() {
		A[0][0] = 0.7; A[0][1] = 0.7;
		new Viterbi(pi, A, B);
	}
	
	// Sum of probabilities in B's row is not 1
	@Test(expected = IllegalArgumentException.class)
	public void wrongSumOfRowInB() {
		B[0][0] = 0.7; B[0][1] = 0.7;
		new Viterbi(pi, A, B);
	}
	
	// pi's length is not equal to A's length
	@Test(expected = IllegalArgumentException.class)
	public void viterbiWrongPi() {
		pi = new double[3];
		new Viterbi(pi, A, B);
	}

	// pi has invalid probability
	@Test(expected = IllegalArgumentException.class)
	public void wrongProbInPi() {
		pi[0] = -0.7; pi[1] = 1.7;
		new Viterbi(pi, A, B);
	}
	
	// Sum of probabilities in pi is not 1
	@Test(expected = IllegalArgumentException.class)
	public void wrongSumOfPi() {
		pi[0] = 0.7;
		pi[1] = 0.7;
		new Viterbi(pi, A, B);
	}
	
	// invalid omega - null omega
	@Test(expected = IllegalArgumentException.class)
	public void omegaNullException() {
		int[] omega = null;
		v.maxScore(omega, true);
	}
	
	// invalid omega - empty omega
	@Test(expected = IllegalArgumentException.class)
	public void omegaEmptyException() {
		int[] omega = {};
		v.bestSeq(omega, true);
	}
	
	// invalid sequence input to probOfSeq - null sequence
	@Test(expected = IllegalArgumentException.class)
	public void probOfSeqNullException() {
		int[] omega = {0, 0, 0, 1};
		List<Integer> seq = null;
		v.probOfSeq(omega, seq, false);
	}
	
	// invalid sequence input to probOfSeq - null sequence
	@Test(expected = IllegalArgumentException.class)
	public void probOfSeqEmptyException() {
		int[] omega = { 0, 0, 0, 1 };
		List<Integer> seq = new LinkedList<>();
		v.probOfSeq(omega, seq, false);
	}
	
	// invalid sequence input to probOfSeq - sequence length is not equal to omega's
	@Test(expected = IllegalArgumentException.class)
	public void probOfSeqLengthException() {
		int[] omega = { 0, 0, 0, 1 };
		List<Integer> seq = new LinkedList<>();
		seq.add(0);
		v.probOfSeq(omega, seq, false);
	}
	
	// Negative k input to maxScoreSet
	@Test(expected = IllegalArgumentException.class)
	public void maxScoreSetException() {
		int[] omega = { 0, 0, 0, 1 };
		v.maxScoreSet(omega, -1, false);
	}
	
	// k is larger than n in maxScoreSet
	@Test(expected = IllegalArgumentException.class)
	public void maxScoreSetException2() {
		int[] omega = { 0, 0, 0, 1 };
		v.maxScoreSet(omega, 3, false);
	}
	
	// k is larger than n^T in maxScoreSetEC
	@Test(expected = IllegalArgumentException.class)
	public void maxScoreSetECException() {
		int[] omega = { 0, 0, 0, 1 };
		v.maxScoreSetEC(omega, 17, false);
	}
	
	// k exceeds number of distinct probabilities
	@Test (expected = IllegalArgumentException.class) 
	public void maxScoreSetException3() {
		double[] pi = {(double) 1/2, (double) 1/2};
		A = new double[2][2];
		A[0][0] = 0; A[0][1] = 1;
		A[1][0] = 0; A[1][1] = 1;
		B = new double[2][3];
		B[0][0] = (double) 1/3; B[0][1] = (double) 1/3; B[0][2] = (double) 1/3;
		B[1][0] = (double) 1/3; B[1][1] = (double) 1/3; B[1][2] = (double) 1/3;
		v = new Viterbi(pi, A, B);
		int[] omega = {0, 0, 0, 0};
		
		v.maxScoreSet(omega, 6, true);
	}
	
	// k exceeds number of possible probabilities
	@Test (expected = IllegalArgumentException.class) 
	public void maxScoreSetECException2() {
		double[] pi = {(double) 1/2, (double) 1/2};
		A = new double[2][2];
		A[0][0] = 0; A[0][1] = 1;
		A[1][0] = 0; A[1][1] = 1;
		B = new double[2][3];
		B[0][0] = (double) 1/3; B[0][1] = (double) 1/3; B[0][2] = (double) 1/3;
		B[1][0] = (double) 1/3; B[1][1] = (double) 1/3; B[1][2] = (double) 1/3;
		v = new Viterbi(pi, A, B);
		int[] omega = {0, 0, 0, 0};
		
		v.maxScoreSetEC(omega, 60, true);
	}
}