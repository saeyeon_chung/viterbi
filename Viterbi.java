import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.PriorityQueue;

public class Viterbi {
	private double[] pi;
	private double[][] A;
	private double[][] B;
	
	/** Creates a new Viterbi-type gsm */
	public Viterbi(double[] pi, double[][] A, double[][] B) {
		// checks if A, B, and pi have valid dimensions
		if (A.length != A[0].length) {
			throw new java.lang.IllegalArgumentException("A is not a square matrix.");
		}
		if (B.length != A.length) {
			throw new java.lang.IllegalArgumentException("Dimension of B is invalid.");
		}
		if (pi.length != A.length) {
			throw new java.lang.IllegalArgumentException("Dimensions of pi do not match with A.");
		}
		
		// checks if A and B represents proper vector probabilities
		double sumRow;
		double prob;
		for (int i = 0; i < A.length; i++) {
			sumRow = 0;
			for (int j = 0; j < A[0].length; j++) {
				prob = A[i][j];
				if (prob < 0 || prob > 1) {
					throw new java.lang.IllegalArgumentException("A has an invalid probability.");
				}
				sumRow += prob;
			}
			if (Math.abs(sumRow - 1) > Math.pow(10, -15)) {
				throw new java.lang.IllegalArgumentException("A is not a proper vector probabilities.");
			}
		}
		for (int i = 0; i < B.length; i++) {
			sumRow = 0;
			for (int j = 0; j < B[0].length; j++) {
				prob = B[i][j];
				if (prob < 0 || prob > 1) {
					throw new java.lang.IllegalArgumentException("B has an invalid probability.");
				}
				sumRow += prob;
			}
			if (Math.abs(sumRow - 1) > Math.pow(10, -15)) {
				throw new java.lang.IllegalArgumentException("B isn't a proper vector probabilities.");
			}
		}
		
		// checks if pi represents proper vector probabilities
		sumRow = 0;
		for (double d : pi) {
			if (d < 0 || d > 1) {
				throw new java.lang.IllegalArgumentException("Pi has an invalid probability.");
			}
			sumRow += d;
		}
		if (Math.abs(sumRow - 1) > Math.pow(10, -15)) {
			throw new java.lang.IllegalArgumentException("Pi is not a proper vector probabilities.");
		}
		
		this.pi = pi;
		this.A = A;
		this.B = B;
	}
	
	/** Computes and prints the probability of a particular sequence of occurring */
	public double probOfSeq(int[] omega, List<Integer> seq, boolean underflow) {
		omegaCheck(omega);
		if (seq == null || seq.size() == 0) {
			throw new java.lang.IllegalArgumentException("The sequence is null or empty.");
		}
		if (seq.size() != omega.length) {
			throw new java.lang.IllegalArgumentException("Size of sequence does not match omega.");
		}
		
		double tprob;
		
		// computes score for t = 1
		if (underflow) tprob = Math.log(pi[seq.get(0)]) + Math.log(B[seq.get(0)][omega[0]]);
		else tprob = pi[seq.get(0)] * B[seq.get(0)][omega[0]];
		
		// computes score for t = 2 to T
		for (int t = 1; t < omega.length; t++) {
			if (underflow) {
				tprob += Math.log(A[seq.get(t-1)][seq.get(t)]) + Math.log(B[seq.get(t)][omega[t]]);
			}
			else {
				tprob *= A[seq.get(t-1)][seq.get(t)] * B[seq.get(t)][omega[t]];
			}
		}
		
		// prints and returns the probability of the sequence
		System.out.println(tprob);
		return tprob;
	}
	
	/** Computes and prints the sequence with the highest probability of occurring */
	public List<Integer> bestSeq(int[] omega, boolean underflow) {
		omegaCheck(omega);
		List<Integer> bestSeq = seqHelper(omega, underflow, true);
		for (int i = 0; i < bestSeq.size(); i++) {
			if (i == bestSeq.size() - 1) System.out.println(bestSeq.get(i));
			else 						 System.out.print(bestSeq.get(i) + ",");
		}
		System.out.println();
		return bestSeq;
	}
	
	/** Computes and prints the sequence with the lowest probability of occurring */
	public List<Integer> worstSeq(int[] omega, boolean underflow) {
		omegaCheck(omega);
		List<Integer> worstSeq = seqHelper(omega, underflow, false);
		for (int i = 0; i < worstSeq.size(); i++) {
			if (i == worstSeq.size() - 1) System.out.println(worstSeq.get(i));
			else 						  System.out.print(worstSeq.get(i) + ",");
		}
		System.out.println();
		return worstSeq;
	}
	
	/** for t = 1, …, T and j = 1, …, n, compute predl(j,t) to be the lowest index l
	 *  such that tscore(l) (at time t) is maximum, and predr(j, t) to be the largest
	 *  index r such that tscore(r) (at time t) is maximum. If l = r, then predl(j,t) =
	 *  predr(j,t). 
	 *  Compute and print the sequence with the highest probability
	 *  corresponding to the indices in predl, and the sequence with the highest
	 *  probability corresponding to the indices in predr. */
	public List<List<Integer>> bestSeqSet(int[] omega, boolean underflow) {
		omegaCheck(omega);
		
		int[][] predL = new int[A.length][omega.length];
		int[][] predR = new int[A.length][omega.length];
		
		LinkedList<List<Integer>> bestSeqSet = new LinkedList<List<Integer>>();
		
		double[][] score = new double[A.length][omega.length];
		double[] tscore;
		double recordtscore = 0;
		
		for (int j = 0; j < A.length; j++) {			
			if (underflow) score[j][0] = Math.log(pi[j]) + Math.log(B[j][omega[0]]);
			else score[j][0] = pi[j] * B[j][omega[0]];
		}
		
		int lowestIndex;
		int highestIndex;
		
		for (int t = 1; t < omega.length; t++) {	
			for (int j = 0; j < A.length; j++) {	
				tscore = new double[A.length];
				lowestIndex = -1;
				highestIndex = -1;
				recordtscore = Double.NEGATIVE_INFINITY;
				
				for (int k = 0; k < A.length; k++) {
					if (underflow) tscore[k] = score[k][t-1] + Math.log(A[k][j]) + Math.log(B[j][omega[t]]);
					else tscore[k] = score[k][t-1] * A[k][j] * B[j][omega[t]];
					// if tscore[k] is greater than max, reset lowestindex and highestindex to this index
					if (tscore[k] > recordtscore) {
						recordtscore = tscore[k];
						lowestIndex = k;
						highestIndex = k;
					}
					// if tscore[k] is same as the max, reset highestindex to this index
					else if (tscore[k] == recordtscore) {
						recordtscore = tscore[k];
						highestIndex = k;
					}
				}
				score[j][t] = recordtscore;	
				predL[j][t] = lowestIndex;
				predR[j][t] = highestIndex;
			}
		}
		
		// for all states at t = T, find the index with max score and put that at the end of sequence
		double maxScoreAtT = Double.NEGATIVE_INFINITY;
		int maxScoreTIndexL = -1;  	// max score's lower index at t = T
		int maxScoreTIndexR = -1;  	// max score's higher index at t = T 
		for (int j = 0; j < A.length; j++) {
			// if score is higher than max score, reset both L and R indices
			if (score[j][omega.length - 1] > maxScoreAtT) {
				maxScoreAtT = score[j][omega.length - 1];
				maxScoreTIndexL = j;
				maxScoreTIndexR = j;
			}
			// else if score is the same as max score, reset only R index
			else if (score[j][omega.length - 1] == maxScoreAtT) {
				maxScoreAtT = score[j][omega.length - 1];
				maxScoreTIndexR = j;
			}
		}
		
		// If at t = T we have two paths that have probability of 0.0, we should return empty list
		if (maxScoreAtT == 0.0 || maxScoreAtT == Double.NEGATIVE_INFINITY) return bestSeqSet;

		// List for low and high index paths and add last state to each
		LinkedList<Integer> bestPathLeft = new LinkedList<Integer>();
		LinkedList<Integer> bestPathRight = new LinkedList<Integer>();
		bestPathLeft.add(maxScoreTIndexL);
		bestPathRight.add(maxScoreTIndexR);
		
		// variables for backtracking sequences 
		int prevIndexL = maxScoreTIndexL;
		int prevIndexR = maxScoreTIndexR;
		int currIndexL;
		int currIndexR;
		
		// backtrack the sequence for left
		for (int t = omega.length - 1; t >= 1; t--) {
			currIndexL = predL[prevIndexL][t];
			bestPathLeft.addFirst(currIndexL);
			prevIndexL = currIndexL;
		}
		
		// prints best path with lowest index
		for (int i = 0; i < bestPathLeft.size(); i++) {
			if (i == bestPathLeft.size() - 1) System.out.print(bestPathLeft.get(i));
			else 						 	  System.out.print(bestPathLeft.get(i) + ",");
		}
		System.out.println();
		bestSeqSet.add(bestPathLeft);
		
		// if lowest and highest index paths are distinct, print and add right path as well
		if (maxScoreTIndexL != maxScoreTIndexR) {
			// backtrack the sequence for right
			for (int t = omega.length - 1; t >= 1; t--) {
				currIndexR = predR[prevIndexR][t];
				bestPathRight.addFirst(currIndexR);
				prevIndexR = currIndexR;
			}
			
			// prints best path with highest index
			for (int i = 0; i < bestPathRight.size(); i++) {
				if (i == bestPathRight.size() - 1) System.out.println(bestPathRight.get(i));
				else 							   System.out.print(bestPathRight.get(i) + ",");
			}
			System.out.println();
			bestSeqSet.add(bestPathRight);
		}
		
		return bestSeqSet;
	}

	/** Computes and prints the highest probability found at time T */
	public double maxScore(int[] omega, boolean underflow) {
		omegaCheck(omega);
		double[][] score = scoreArray(omega, underflow);
		double maxScoreAtT = Double.NEGATIVE_INFINITY;

		// for all states at t = T, find max probability
		for (int j = 0; j < A.length; j++) {
			if (score[j][omega.length - 1] > maxScoreAtT) {
				maxScoreAtT = score[j][omega.length - 1];
			}
		}
		
		System.out.println(maxScoreAtT);
		return maxScoreAtT;
	}
	
	/** Naive version: Computes and prints the k distinct highest probabilities found at time T */
	public double[] maxScoreSet(int[] omega, int k, boolean underflow) {
		omegaCheck(omega);

		// If k is negative or greater than number of states, throw IAE
		if (k < 0) {
			throw new java.lang.IllegalArgumentException("k should be a positive integer.");
		}
		
		// collect all highest probabilities from 
		double[][] score = scoreArray(omega, underflow);
		List<Double> scoreT = new LinkedList<Double>();
		for (int i = 0; i < A.length; i++) {
			if (!scoreT.contains(score[i][omega.length-1])) {
				scoreT.add(score[i][omega.length-1]);
			}
		}
		
		// if k is larger than the number of distinct probabilities at time T, throw an exception
		if (k > scoreT.size()) {
			throw new java.lang.IllegalArgumentException("k greater than number distinct probabilities.");
		}
		
		Collections.sort(scoreT, Collections.reverseOrder());
		
		double[] maxScoreSet = new double[k];
		int counter = 0;
		while (counter < k) {
			maxScoreSet[counter] = scoreT.get(counter);
			counter++;
		}

		for (int i = 0; i < maxScoreSet.length; i++) {
			if (i == maxScoreSet.length - 1) System.out.println(maxScoreSet[i]);
			else System.out.print(maxScoreSet[i] + ",");
		}
		System.out.println();
		
		return maxScoreSet;
	}
	
	/** Rigorous version: Computes and prints the k distinct highest probabilities found at time T */
	public double[] maxScoreSetEC(int[] omega, int k, boolean underflow) {
		omegaCheck(omega);
		
		// If k is negative or greater than possible probabilities (n^T), throw IAE
		if (k < 0) {
			throw new java.lang.IllegalArgumentException("k should be a positive integer.");
		}
		if (k > Math.pow(A.length, omega.length)) {
			throw new java.lang.IllegalArgumentException("k > number of possible probabilities.");
		}
		
		PriorityQueue<Prob> pq = new PriorityQueue<Prob>();
		
		// t = 0 for all j's and add to pq
		for (int j = 0; j < A.length; j++) {	
			Prob p;
			if (underflow) p = new Prob((Math.log(pi[j]) + Math.log(B[j][omega[0]])), j);
			else 		   p = new Prob(pi[j] * B[j][omega[0]], j);
			pq.add(p);
			if (pq.size() > k) pq.remove();
		}
		
		// for t = 1 to T, for each Prob in the queue, add transitions to other states to new Queue 
		for (int t = 1; t < omega.length; t++) {
			PriorityQueue<Prob> pqTemp = new PriorityQueue<Prob>();
			for (Prob p : pq) {
				for (int j = 0; j < A.length; j++) {
					double newP;
					if (underflow) {
						newP = p.probability + Math.log(A[p.prevIndex][j]) + Math.log(B[j][omega[t]]);
	                }
					else {
						newP = p.probability * A[p.prevIndex][j] * B[j][omega[t]];
					}
					Prob newProb = new Prob(newP, j);
					// adds new Prob to the queue, checking duplicate and size of queue
					if (!pqTemp.contains(newProb)) pqTemp.add(newProb);
	                if (pqTemp.size() > k) pqTemp.remove();
				}
			}
			pq = pqTemp;
		}
		
		// array to be returned - only contains probabilities from the queue
		double[] maxScoreSet = new double[k];
		for (int i = pq.size()-1; i >= 0; i--) {
			maxScoreSet[i] = pq.remove().probability;
		}
		for (int i = 0; i < maxScoreSet.length; i++) {
			if (i == maxScoreSet.length - 1) System.out.print(maxScoreSet[i]);
			else System.out.print(maxScoreSet[i] + ",");
		}
		System.out.println();
		return maxScoreSet;
	}
	
	/************************************** HELPER FUNCTIONS **************************************/
	
	/** Helper method that checks if omega is null or empty and throws IAE */
	private void omegaCheck(int[] omega) {
		if (omega == null || omega.length == 0) {
			throw new java.lang.IllegalArgumentException("Omega is null or empty.");
		}
	}
	
	/** Helper method that finds the best/worst sequence given omega, and returns the list */
	private List<Integer> seqHelper(int[] omega, boolean underflow, boolean max) {
		int[][] preds = new int[A.length][omega.length];
		LinkedList<Integer> recordPath = new LinkedList<Integer>(); 
		
		int recordIndex;
		double[][] score = new double[A.length][omega.length];
		double[] tscore;
		double recordtscore = 0;
		
		for (int j = 0; j < A.length; j++) {			
			if (underflow) score[j][0] = Math.log(pi[j]) + Math.log(B[j][omega[0]]);
			else score[j][0] = pi[j] * B[j][omega[0]];
		}
		
		for (int t = 1; t < omega.length; t++) {	
			for (int j = 0; j < A.length; j++) {	
				tscore = new double[A.length];
				recordIndex = -1;
				
				if (max) recordtscore = Double.NEGATIVE_INFINITY;
				else 	 recordtscore = Double.POSITIVE_INFINITY;
				
				for (int k = 0; k < A.length; k++) {
					if (underflow) {
						tscore[k] = score[k][t-1] + Math.log(A[k][j]) + Math.log(B[j][omega[t]]);
					}
					else {
						tscore[k] = score[k][t-1] * A[k][j] * B[j][omega[t]];
					}
					
					if (max) {
						if (tscore[k] > recordtscore) {
							recordtscore = tscore[k];
							recordIndex = k;
						}
					}
					
					else {	// find min path
						if (tscore[k] < recordtscore) {
							recordtscore = tscore[k];
							recordIndex = k;
						}
					}
				}
				score[j][t] = recordtscore;	
				if (recordIndex == -1) preds[j][t] = 0;
				else preds[j][t] = recordIndex;
			}
		}
		
		double recordScoreAtT;
		if (max) recordScoreAtT = Double.NEGATIVE_INFINITY;
		else 	 recordScoreAtT = Double.POSITIVE_INFINITY;
		int recordScoreTIndex = -1;
		double scoreAtT;
		
		// for all states at t = T, find the index with max or min score and put that at the end of sequence 
		for (int j = 0; j < A.length; j++) {
			scoreAtT = score[j][omega.length-1];
			if (max) {
				if (scoreAtT > recordScoreAtT) {
					recordScoreAtT = scoreAtT;
					recordScoreTIndex = j;
				}
			}
			else {
				if (scoreAtT < recordScoreAtT) {
					recordScoreAtT = scoreAtT;
					recordScoreTIndex = j;
				}
			}
		}
		
		// NEW VERSION - -INFINITY & 0.0 -> START AT INDEX 0
		if (recordScoreTIndex == -1) recordScoreTIndex = 0;
		
		recordPath.add(recordScoreTIndex);
		int prevIndex = recordScoreTIndex;
		int currIndex;
		
		// backtrack the sequence
		for (int t = omega.length - 1; t >= 1; t--) {
			currIndex = preds[prevIndex][t];
			recordPath.addFirst(currIndex);
			prevIndex = currIndex;
		}
		
		return recordPath;
	}
	
	/** Helper method that calculates and returns score[][] array */
	private double[][] scoreArray(int[] omega, boolean underflow) {
		double[][] score = new double[A.length][omega.length];
		double[] tscore;
		double maxtscore = 0;
		
		for (int j = 0; j < A.length; j++) {
			if (underflow) score[j][0] = Math.log(pi[j]) + Math.log(B[j][omega[0]]);
			else score[j][0] = pi[j] * B[j][omega[0]];
		}
		
		for (int t = 1; t < omega.length; t++) {
			for (int j = 0; j < A.length; j++) {
				tscore = new double[A.length];
				maxtscore = Double.NEGATIVE_INFINITY;
				for (int k = 0; k < A.length; k++) {
					if (underflow) {
						tscore[k] = score[k][t-1] + Math.log(A[k][j]) + Math.log(B[j][omega[t]]);
					}
					else {
						tscore[k] = score[k][t-1] * A[k][j] * B[j][omega[t]];
					}
					if (tscore[k] > maxtscore) {
						maxtscore = tscore[k];
					}
				}
				score[j][t] = maxtscore;
			}
		}
		return score;
	}
	
	/** Private class for (probability, index) pair used for maxScoreSet Rigorous version */
	private class Prob implements Comparable<Prob> {
		private double probability;
		private int prevIndex;
		
		public Prob(double prob, int i) {
			this.probability = prob;
			this.prevIndex = i;
		}
		
		@Override
		public int compareTo(Prob that) {
			// TODO Auto-generated method stub
			if (probability < that.probability)
				return -1;
			if (probability > that.probability)
				return 1;
			return 0;
		}
		
		@Override
		public boolean equals(Object o) {
	        if (o == this) {
	            return true;
	        }
	        
	        if (!(o instanceof Prob)) {
	            return false;
	        }
	        
	        Prob p = (Prob) o;
	        
			return Math.abs(probability - p.probability) < Math.pow(10, -15);
		}
	}
}